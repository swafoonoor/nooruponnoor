//
//  UserSignUp.swift
//  ParseStarterProject-Swift
//
//  Created by Haki on 3/28/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import Parse
import UIKit

class SignUp: UIViewController {

//BEGIN OF FUNCTION
    func isValidEmail(testStr:String) -> Bool {
        
        print("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
        
    }
    
    
    
    
    func alert(title: String, message:String){
        
        if #available(iOS 8.0, *) {
            let alertpassword = UIAlertController(title: title, message:message, preferredStyle: UIAlertControllerStyle.Alert)
            alertpassword.addAction((UIAlertAction(title: "ok", style: .Default, handler: {(action) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
            })))
            self.presentViewController(alertpassword, animated: true, completion: nil)
            
        } // Fallback on earlier versions
        return
    }
//END OF FUNCTION

    
//Variables
    
    @IBOutlet var userFirstName: UITextField!
    @IBOutlet var userLastName: UITextField!
    @IBOutlet var userPassword: UITextField!
    @IBOutlet var userConfirmPass: UITextField!
    @IBOutlet var userEmail: UITextField!
    @IBOutlet var userMF: UITextField!
    @IBOutlet var userState: UITextField!
    @IBOutlet var userMonth: UITextField!
    @IBOutlet var userDay: UITextField!
    @IBOutlet var userYear: UITextField!
    var errormessage = "Please try again later"
//END OF VARIABLES
    
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
    
    @IBAction func saveUser(sender: AnyObject) {
        
        
        
        
        
        ///Begining of saving Sign up ///
        
        let newUser = PFUser()
        
        newUser.username = userEmail.text
        newUser.password = userPassword.text
        newUser.signUpInBackgroundWithBlock{ (success, error) in
            
            if error == nil{
            
                self.alert("Logged In",message: "sign up successful")
                print("user signed up")
                
                let UserList = PFObject(className: "UserCreation")
                
                UserList ["email"] = self.userEmail.text
                UserList ["FirstName"] = self.userFirstName.text
                UserList ["LastName"] = self.userLastName.text
                UserList ["Password"] = self.userPassword.text
                UserList ["Gender"] = self.userMF.text
                UserList ["State"] = self.userState.text
                UserList ["Month"] = self.userMonth.text
                UserList ["Day"] = self.userDay.text
                UserList ["Year"] = self.userYear.text
                
                UserList.saveInBackground()
            
            }else{
            
                if let errorstring = error?.userInfo["error"] as? String{
                
                self.errormessage = errorstring
                    
                }
                self.alert("Failed Sign up", message: self.errormessage)
            }
        }
        
       
        
        
        
    }
    
    
    
    
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
